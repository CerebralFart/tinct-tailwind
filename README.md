# @tinct/tailwind

The tailwind swatch mirrors the default colors defined in tailwind css and is intended to be used alongside the `tinct` package.

Note that all colors are available as a shade, and as such cannot be combined with shader functions. In other words, `pastelBlue300` would not be a valid color.
However, the 500-variant of each color is also available as a normal color. This enables you to request colors such as `pastelOrange`.

If you have any questions, feel free to open an issue.