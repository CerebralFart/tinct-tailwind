import {Color, Swatch} from "tinct";

const shades = {
	// Primaries
	black: new Color("#000000"),
	white: new Color("#FFFFFF"),

	// Gray
	gray100: new Color("#F7FAFC"),
	gray200: new Color("#EDF2F7"),
	gray300: new Color("#E2E8F0"),
	gray400: new Color("#CBD5E0"),
	gray500: new Color("#A0AEC0"),
	gray600: new Color("#718096"),
	gray700: new Color("#4A5568"),
	gray800: new Color("#2D3748"),
	gray900: new Color("#1A202C"),

	// Red
	red100: new Color("#FFF5F5"),
	red200: new Color("#FED7D7"),
	red300: new Color("#FEB2B2"),
	red400: new Color("#FC8181"),
	red500: new Color("#F56565"),
	red600: new Color("#E53E3E"),
	red700: new Color("#C53030"),
	red800: new Color("#9B2C2C"),
	red900: new Color("#742A2A"),

	// Orange
	orange100: new Color("#FFFAF0"),
	orange200: new Color("#FEEBC8"),
	orange300: new Color("#FBD38D"),
	orange400: new Color("#F6AD55"),
	orange500: new Color("#ED8936"),
	orange600: new Color("#DD6B20"),
	orange700: new Color("#C05621"),
	orange800: new Color("#9C4221"),
	orange900: new Color("#7B341E"),

	// Yellow
	yellow100: new Color("#FFFFF0"),
	yellow200: new Color("#FEFCBF"),
	yellow300: new Color("#FAF089"),
	yellow400: new Color("#F6E05E"),
	yellow500: new Color("#ECC94B"),
	yellow600: new Color("#D69E2E"),
	yellow700: new Color("#B7791F"),
	yellow800: new Color("#975A16"),
	yellow900: new Color("#744210"),

	// Green
	green100: new Color("#F0FFF4"),
	green200: new Color("#C6F6D5"),
	green300: new Color("#9AE6B4"),
	green400: new Color("#68D391"),
	green500: new Color("#48BB78"),
	green600: new Color("#38A169"),
	green700: new Color("#2F855A"),
	green800: new Color("#276749"),
	green900: new Color("#22543D"),

	// Teal
	teal100: new Color("#E6FFFA"),
	teal200: new Color("#B2F5EA"),
	teal300: new Color("#81E6D9"),
	teal400: new Color("#4FD1C5"),
	teal500: new Color("#38B2AC"),
	teal600: new Color("#319795"),
	teal700: new Color("#2C7A7B"),
	teal800: new Color("#285E61"),
	teal900: new Color("#234E52"),

	// Blue
	blue100: new Color("#EBF8FF"),
	blue200: new Color("#BEE3F8"),
	blue300: new Color("#90CDF4"),
	blue400: new Color("#63B3ED"),
	blue500: new Color("#4299E1"),
	blue600: new Color("#3182CE"),
	blue700: new Color("#2B6CB0"),
	blue800: new Color("#2C5282"),
	blue900: new Color("#2A4365"),

	// Indigo
	indigo100: new Color("#EBF4FF"),
	indigo200: new Color("#C3DAFE"),
	indigo300: new Color("#A3BFFA"),
	indigo400: new Color("#7F9CF5"),
	indigo500: new Color("#667EEA"),
	indigo600: new Color("#5A67D8"),
	indigo700: new Color("#4C51BF"),
	indigo800: new Color("#434190"),
	indigo900: new Color("#3C366B"),

	// Purple
	purple100: new Color("#FAF5FF"),
	purple200: new Color("#E9D8FD"),
	purple300: new Color("#D6BCFA"),
	purple400: new Color("#B794F4"),
	purple500: new Color("#9F7AEA"),
	purple600: new Color("#805AD5"),
	purple700: new Color("#6B46C1"),
	purple800: new Color("#553C9A"),
	purple900: new Color("#44337A"),

	// Pink
	pink100: new Color("#FFF5F7"),
	pink200: new Color("#FED7E2"),
	pink300: new Color("#FBB6CE"),
	pink400: new Color("#F687B3"),
	pink500: new Color("#ED64A6"),
	pink600: new Color("#D53F8C"),
	pink700: new Color("#B83280"),
	pink800: new Color("#97266D"),
	pink900: new Color("#702459"),
};

const tailwind = new Swatch({
	name: 'tailwind',
	colors: {
		gray: shades.gray500,
		red: shades.red500,
		orange: shades.orange500,
		yellow: shades.yellow500,
		green: shades.green500,
		teal: shades.teal500,
		blue: shades.blue500,
		indigo: shades.indigo500,
		purple: shades.purple500,
		pink: shades.pink500,
	},
	shades
});

export default tailwind;